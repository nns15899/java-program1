FROM openjdk:8
COPY . /src/java
WORKDIR /src/java
RUN chmod +x hello.java
EXPOSE 5000
ENTRYPOINT ["/src/java/hello.java"]